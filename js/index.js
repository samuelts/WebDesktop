//Loader fake load (delay 1s)
setTimeout(function(){
  document.body.classList.add('loaded');
}, 500);

// Time
setInterval(getDate, 1000);
var is24h = true;

// Buttons
var osBtn = document.getElementById("os-btn");
var logoutBtn = document.getElementById("logout-btn");
var aboutBtn = document.getElementById("about-btn");
var projectsBtn = document.getElementById("projects-btn");
var contactBtn = document.getElementById("contact-btn");
var closeBtns = [...document.getElementsByClassName("close-btn")];

// Display Elements
var header = document.getElementById("header");
var dock = document.getElementById("dock");
var dockLabel = document.getElementById("dock-label");
var osMenu = document.getElementById("os-menu");
var loginDiv = document.getElementById("login");
var about = document.getElementById("about");
var projects = document.getElementById("projects");
var contact = document.getElementById("contact");

// Draggable elements
var draggable = [about, projects, contact];

// Event handlers
loginDiv.onclick = login;
logoutBtn.onclick = logout;
osBtn.onclick = toggleOSMenu;
closeBtns.map((x) => x.onclick = closeWindow);

var dockBtns = [aboutBtn, projectsBtn, contactBtn];
dockBtns.map((x) => {
  x.onmouseover = updateDockLabel;
  x.onmouseout = clearDockLabel;
});

// Make these elements draggable
draggable.map((x) => dragElement(x));

// Login and show elements
function login() {
  header.classList.remove("header-hide");
  dock.classList.remove("dock-hide");
  loginDiv.classList.add("logged-in");
}

function logout() {
  toggleOSMenu();
  draggable.map((x) => hide(x));
  header.classList.add("header-hide");
  dock.classList.add("dock-hide");
  loginDiv.classList.remove("logged-in");
}

function updateDockLabel() {
  let e = event;
  if (e.target.id == "about-btn" || e.target.parentElement.id == "about-btn") {
    dockLabel.innerText = "About Me";
    dockLabel.style.padding = "0.25em";
  } else if (e.target.id == "projects-btn" || e.target.parentElement.id == "projects-btn") {
    dockLabel.innerText = "Projects";
    dockLabel.style.padding = "0.25em";
  } else if (e.target.id == "contact-btn" || e.target.parentElement.id == "contact-btn") {
    dockLabel.innerText = "Contact Me";
    dockLabel.style.padding = "0.25em";
  }
}

function clearDockLabel() {
  dockLabel.innerText = "";
  dockLabel.style.padding = "0";
}

// Toggle 24/12 hr switch
function toggle24() {
  is24h = !is24h;
  getDate();
}

// Get the current time
function getDate() {
  const date = new Date();

  const hours = date.getHours();
  const hours12h = hours > 12 ? hours - 12 : hours;
  const hourStr = hours < 10 ? `0${hours}` : `${hours}`;
  const hour12hStr = `${hours12h}`;

  const minutes = date.getMinutes();
  const minuteStr = minutes < 10 ? `0${minutes}` : `${minutes}`;
  const minute12hStr = `${minuteStr} ${hours >= 12 ? 'PM' : 'AM'}`;

  const seconds = date.getSeconds();
  const secondStr = seconds < 10 ? `0${seconds}` : `${seconds}`;

  const colcol = seconds % 2 == 0 ? "transparent" : "#eee";

  document.getElementById("hours").innerText =  is24h ? hourStr : hour12hStr;
  document.getElementById("minutes").innerText = is24h ? minuteStr : minute12hStr;
  document.getElementById("colon").style.color = colcol;
}

// Toggle main menu
function toggleOSMenu() {
  if (osMenu.classList.contains("menu-open")) {
    osMenu.classList.remove("menu-open");
    osMenu.classList.add("menu-closed");
    osBtn.innerHTML = `<i class="fas fa-caret-down"></i> menu`;
  } else {
    osMenu.classList.remove("menu-closed");
    osMenu.classList.add("menu-open");
    osBtn.innerHTML = `<i class="fas fa-caret-up"></i> menu`;
  }
}

// Toggle visible class
function restore(item) {
  item.classList.add("visible");
  item.classList.remove("hidden");
}

// Toggle hidden class
function hide(item) {
  item.classList.remove("visible");
  item.classList.add("hidden");
}

// Close the window (slide off screen)
function closeWindow() {
  let rootElem = this.parentElement.parentElement;
  hide(rootElem);
  rootElem.style.zIndex = "1";
  setTimeout(() => {
    rootElem.style.top = "";
    rootElem.style.left = "";
  }, 1000);
}

// Bring window to focus
function adjustFocus(target) {
  draggable.map((x) => {
    if (x === target) {
      if (x.classList.contains("hidden")) {
        restore(x);
      }
      x.style.zIndex = "3";
    } else {
      x.style.zIndex = "2";
    }
  });
  if (osMenu.classList.contains("menu-open")) {
    toggleOSMenu();
  }
}

// Make windows draggable
function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  document.getElementById(elmnt.id + "-header").onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    adjustFocus(e.target.parentElement);
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
